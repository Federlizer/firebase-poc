use jsonwebtoken::Algorithm;
use jsonwebtoken::Validation;
use serde::{Serialize, Deserialize};
use jsonwebtoken::DecodingKey;
use jsonwebtoken::decode;

#[derive(Serialize, Deserialize)]
struct Claims {
    iss: String,
    aud: String,
    sub: String,
}

fn main() {
    // Authenticate on backend:
    // https://firebase.google.com/docs/auth/admin/verify-id-tokens#verify_id_tokens_using_a_third-party_jwt_library
    // We must be sending the JWT (idToken) to the backend. That looks to be enough.

    let pub_key_url = "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com";

    // Some random public key - not the actual pubkey from google firebase - this is used to do the PoC
    let pub_key_str = "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo
4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u
+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh
kd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ
0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg
cKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc
mwIDAQAB
-----END PUBLIC KEY-----";

    // Difference between good_jw_token_str and bad_jwt_token_str is only at the payload section.
    // The good token contains the information that was contained during creation of the token (+ correct signature)
    // On the other hand, the bad token is the same token, but with the payload section tampered with. This means that the signature wasn't re-calculated, resulting in a bad token.
    // Uncomment one that you'd like to test with

    // Good jwt_token_str
    let jwt_token_str     = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZmlyLXBvYy1kZTY1ZSIsImF1ZCI6ImZpci1wb2MtZGU2NWUiLCJhdXRoX3RpbWUiOjE2ODY5OTgyNTMsInVzZXJfaWQiOiJXeEJQTVlmMUNNUDVKVzk5dVZmdE5BMlRFRDAzIiwic3ViIjoiV3hCUE1ZZjFDTVA1Slc5OXVWZnROQTJURUQwMyIsImlhdCI6MTY4Njk5ODI1MywiZXhwIjoxNzg3MDAxODUzLCJlbWFpbCI6ImZlZGVybGl6ZXJAcHJvdG9ubWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiZmVkZXJsaXplckBwcm90b25tYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.YtzGeoPt275VBvxuYZZYXkndXF_mMeekIaiJUlm9rjKQvFUKw5FfTZicVr49ziAzTItUMovF2hItCeyUiXAKrl2IAwDXfp_TciqybTdIqQymu0UjjH_d0zZeKxnVSDSlJHRX5KhNfakS35WiL7udXyZJNE6xpJKGlJ6k7dk4FmrSWCs-y1nir-iY3W46JC1G_snmz2SFxMWXvbN1Jio1Lj9xmKlNwvy0xvUhq5FtgYgv-5K7AxMXUWQXmrBjjAMn1ae6-AAMwFiiFb__KNdT7OFpe0Fd_ObYWn0cZPt6bvVE7Na-xtEx-EMg9Ku_ajF_MtMZh4YtcyO723LinYLFQw";

    // Bad jwt_token_str
    // let jwt_token_str     = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZmlyLXBvYy1kZTY1ZSIsImF1ZCI6ImZpci1wb2MtZGU2NWUiLCJhdXRoX3RpbWUiOjE2ODY5OTgyNTMsInVzZXJfaWQiOiJXeEJQTVlmMUNNUDVKVzk5dVZmdE5BMlRFRDAzIiwic3ViIjoiV3hCUE1ZZjFDTVA1Slc5OXVWZnROQTJURUQwMyIsImlhdCI6MTY4Njk5ODI1MywiZXhwIjoxODg3MDAxODUzLCJlbWFpbCI6ImZlZGVybGl6ZXJAcHJvdG9ubWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiZmVkZXJsaXplckBwcm90b25tYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19Cg.YtzGeoPt275VBvxuYZZYXkndXF_mMeekIaiJUlm9rjKQvFUKw5FfTZicVr49ziAzTItUMovF2hItCeyUiXAKrl2IAwDXfp_TciqybTdIqQymu0UjjH_d0zZeKxnVSDSlJHRX5KhNfakS35WiL7udXyZJNE6xpJKGlJ6k7dk4FmrSWCs-y1nir-iY3W46JC1G_snmz2SFxMWXvbN1Jio1Lj9xmKlNwvy0xvUhq5FtgYgv-5K7AxMXUWQXmrBjjAMn1ae6-AAMwFiiFb__KNdT7OFpe0Fd_ObYWn0cZPt6bvVE7Na-xtEx-EMg9Ku_ajF_MtMZh4YtcyO723LinYLFQw";


    let key = match DecodingKey::from_rsa_pem(pub_key_str.as_bytes()) {
        Ok(key) => key,
        Err(err) => panic!("Unable to convert rsa pem key: {err}"),
    };

    println!("Got key!");

    let decoded_token = match decode::<Claims>(jwt_token_str, &key, &Validation::new(Algorithm::RS256)) {
        Ok(tkn) => tkn,
        Err(err) => panic!("Unable to decode token: {err}"),
    };

    println!("decoded the token as well!");
    println!("iss: {}", decoded_token.claims.iss);
    println!("aud: {}", decoded_token.claims.aud);
    println!("sub: {}", decoded_token.claims.sub);

    // Fetch public keys
}
