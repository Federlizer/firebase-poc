# Firebase Proof of Concept

Just a short project for a PoC of using Firebase in a frontend-backend application.

### Frontend

Install node_modules (the heaviest thing in the universe):
```
$ npm install
```

Remember to update the firebaseConfig file with your own firebase configuration (use firebase console for that).
```javascript
// src/index.js

...

const firebaseConfig = {
  apiKey: "apiKey",
  authDomain: "authDomain",
  projectId: "projectId",
  storageBucket: "storageBucket",
  messagingSenderId: "messagingSenderId",
  appId: "appId"
};

...

```

Run with `npm run start` to print the full userCredential object to the screen. We're looking for the `tokenId` field -
that's the one that contains the JWT that's going to be decoded and verified by the backend.

### Backend

Install rust + cargo
(https://www.rust-lang.org/tools/install)

Nah, fuck it, you won't be doing that anyways :D
