import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";

// Fill in with own firebaseConfig
const firebaseConfig = {
  apiKey: "apiKey",
  authDomain: "authDomain",
  projectId: "projectId",
  storageBucket: "storageBucket",
  messagingSenderId: "messagingSenderId",
  appId: "appId"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

/*
createUserWithEmailAndPassword(auth, "my_email@test.com", "test1234")
    .then((userCredentials) => {
        console.log(userCredentials);
    })
    .catch((err) => {
        console.log("Received an error when creating user");
        console.log(err);
    })
*/

signInWithEmailAndPassword(auth, "my_email@test.com", "test1234")
    .then((userCredentials) => {
        console.log(userCredentials);
    })
    .catch((err) => {
        console.log("Received an error when signing in");
        console.log(err);
    })
